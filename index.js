const express = require('express')
const compression = require('compression')
const app = express()

// const smtp = require('./src/config/email')
const server = require('./src/config/server')
const router = require('./src/routers')
const logger = require('./src/libs/morgan')
const port = process.env.PORT

app.use(compression())
app.use(express.urlencoded({ extended: true }))
app.use(express.json())
app.use(logger)
app.use(router)

server({ app, port }, async (serv) => {
    try {
        serv.Listening()
    } catch (error) {
        console.log(error)
    }
})
