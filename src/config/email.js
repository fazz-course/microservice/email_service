const nodemailer = require('nodemailer')

module.exports = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        type: 'OAuth2',
        clientId: process.env.GDEV_CLIENT_ID,
        clientSecret: process.env.GDEV_CLIENT_SECRET
    }
})
