const http = require('http')
const loggs = require('../libs/logger')(module)

module.exports = ({ app, port }, cb) => {
    const server = {}
    const httpServer = http.createServer(app)

    server.Listening = () => {
        httpServer.listen(port)
        httpServer.on('listening', server.onListening)
    }

    server.onListening = () => {
        let bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + port
        loggs.info('Listening on ' + bind)
    }

    cb(server)
}
