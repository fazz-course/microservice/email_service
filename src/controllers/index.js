const ctrl = {}
const logss = require('../libs/logger')(module)
const mail = require('../config/email')

ctrl.SendEmail = (req, res) => {
    const { email, token } = req.query

    mail.on('token', (token) => {
        console.log('User: %s', token.user)
        console.log('Access Token: %s', token.accessToken)
        console.log('Expires: %s', new Date(token.expires))
    })

    const MailOptions = {
        from: `"Fazz Course 👻" <${process.env.EM_USERNAME}>`,
        to: email,
        subject: 'Account Verification',
        text: 'Test Text',
        html: `
        <table align="center"><tr><td align="center" width="9999">
        <h2 align="center">Click Button untuk Verifikasi</h2>
        <img src="https://res.cloudinary.com/antikey/image/upload/v1667849615/logofazz_ojpnt2.jpg" align="center" width="150" alt="Project icon">

        <div align="center">
            <a href="http://localhost:2021/api/auth/verify/${token}" target="blank" style="
                background:#FF9933;
                cursor:pointer;
                border: none;
                color: white;
                border-radius: 4%;
                padding: 8px 18px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 14px;
                "
             >click Here</a>
        </div>

        email from email service, see repository here  <a href="https://gitlab.com/fazz-course" target="blank">here..</a>

        </td></tr></table>
        `,
        auth: {
            user: process.env.GDEV_EMAIL,
            refreshToken: process.env.REF_TOKEN,
            accessToken: process.env.ACS_TOKEN,
            expires: 1494388182480
        }
    }

    mail.sendMail(MailOptions, (err, info) => {
        if (err) {
            logss.error(err)
            return res.status(500).send({ message: err.message })
        }

        return res.status(200).send({ message: `succsess sending email to ${email}` })
    })
}

module.exports = ctrl
