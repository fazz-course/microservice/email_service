const express = require('express')
const routers = express.Router()
const ctrl = require('../controllers')

routers.post('/', ctrl.SendEmail)

module.exports = routers
